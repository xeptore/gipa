package config

import (
	"errors"
	"fmt"
	"io/ioutil"

	"gitlab.com/xeptore/gipa/types"

	"gopkg.in/yaml.v2"
)

const configFileName = "config.yml"

func ParseConfigs(config *types.Conf) error {
	configFile, err := ioutil.ReadFile("config/config.yml")
	if err != nil {
		return errors.New(fmt.Sprintf("unable to read %q\n", config))
	}

	// parse config file
	err = yaml.Unmarshal(configFile, &(*config))
	if err != nil {
		return errors.New(fmt.Sprintf("invalid config file synatx: %s\n", err.Error()))
	}
	return nil
}
