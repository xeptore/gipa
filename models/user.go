package models

import (
	"errors"
	"regexp"
	"strconv"
	"sync"

	"gitlab.com/xeptore/gipa/types"

	"github.com/badoux/checkmail"
)

const (
	Male   string = "m"
	Female string = "f"
)

type User struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Mobile    string `json:"mobile"`
	Age       int    `json:"age"`
	Gender    string `json:"gender"`
	Job       string `json:"job"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

type ResponseError types.ResponseError

func (u *User) Validate() (bool, *ResponseError) {
	var outErr = make(ResponseError)
	hasError := false
	var wg sync.WaitGroup
	wg.Add(6)
	go validateFirstname(u.FirstName, &outErr, &wg)
	go validateLastname(u.LastName, &outErr, &wg)
	go validateAge(u.Age, &outErr, &wg)
	go validateGender(u.Gender, &outErr, &wg)
	go validateJob(u.Job, &outErr, &wg)
	go validateEmail(u.Email, &outErr, &wg)
	wg.Wait()
	for _, v := range outErr {
		if len(v) != 0 {
			hasError = true
			break
		}
	}
	return hasError, &outErr
}

func validateFirstname(f string, errStore *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	if err := name(f); err != nil {
		(*errStore)["first_name"] = err.Error()
	}
}

func validateLastname(l string, errStore *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	if err := name(l); err != nil {
		(*errStore)["last_name"] = err.Error()
	}
}

func name(f string) error {
	if len(f) != 0 {
		if matched, err := regexp.MatchString("[0-9]", f); err != nil || matched {
			return errors.New("invalid")
		}
	} else {
		return errors.New("required")
	}
	return nil
}

func validateMobile(m string, out *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	if l := len(m); l != 0 {
		if l != 10 || !toI(m) {
			(*out)["mobile"] = errors.New("invalid").Error()
		}
	} else {
		(*out)["mobile"] = errors.New("required").Error()
	}
}

func validateAge(a int, out *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	age := strconv.Itoa(a)
	if l := len(age); l > 2 || age == "0" {
		(*out)["age"] = errors.New("invalid").Error()
	} else if l == 0 {
		(*out)["age"] = errors.New("required").Error()
	}
}

func toI(a string) bool {
	if n, err := strconv.Atoi(a); err == nil && n >= 0 && strconv.Itoa(n) == a {
		return true
	}
	return false
}

func validateGender(g string, out *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	if len(g) == 0 {
		(*out)["gender"] = errors.New("required").Error()
	} else if g != "m" && g != "f" {
		(*out)["gender"] = errors.New("invalid").Error()
	}
}

func validateJob(j string, out *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	if len(j) == 0 {
		(*out)["job"] = errors.New("required").Error()
	} else if matched, err := regexp.MatchString("[0-9]", j); err != nil || matched {
		(*out)["job"] = errors.New("invalid").Error()
	}
}

func validateEmail(e string, out *ResponseError, wg *sync.WaitGroup) {
	defer wg.Done()
	if len(e) != 0 {
		if err := checkmail.ValidateFormat(e); err != nil {
			(*out)["email"] = errors.New("invalid").Error()
		}
	} else {
		(*out)["email"] = errors.New("required").Error()
	}
}
