package auth

import (
	"database/sql"
	"fmt"
	"net/http"
	"path"
	"time"

	"gitlab.com/xeptore/gipa/container"
	"gitlab.com/xeptore/gipa/controllers"
	"gitlab.com/xeptore/gipa/services/validator"

	"github.com/gin-gonic/gin"
	"github.com/kjk/betterguid"
	"gitlab.com/xeptore/gipa/lib"
)

func Login(c *gin.Context) {
	db := container.DBConnection()
	mobile := c.PostForm("mobile")

	successResStatusCode := http.StatusOK

	err := validator.ValidateMobile(mobile)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid mobile number",
		})
		return
	}

	var userID string
	stmtFindUser, err := db.Prepare("SELECT id FROM users WHERE mobile = $1")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal error",
		})
		return
	}
	defer controllers.CloseSafe(stmtFindUser)
	err = stmtFindUser.QueryRow(mobile).Scan(&userID)
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal error",
		})
		return
	} else if err == sql.ErrNoRows {
		successResStatusCode = http.StatusCreated
		userID = betterguid.New()
		c.Header("Location", "http://"+path.Join(c.Request.Host, "users", userID))
		stmtCreateUser, err := db.Prepare("INSERT INTO users (id, mobile) VALUES ($1, $2)")
		defer controllers.CloseSafe(stmtCreateUser)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "error while creating new user",
			})
			return
		}

		_, err = stmtCreateUser.Exec(userID, mobile)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "error while registering user",
			})
			return
		}
	}

	var (
		prevLoginCode           string
		prevLoginCodeExpiration string
	)
	stmtFindPreviousLoginCode, err := db.Prepare("SELECT login_code, code_generation_date FROM users_login_codes WHERE user_id = $1 ORDER BY id DESC")
	defer controllers.CloseSafe(stmtFindPreviousLoginCode)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error while checking for previous login code",
		})
		return
	}
	err = stmtFindPreviousLoginCode.QueryRow(userID).Scan(&prevLoginCode, &prevLoginCodeExpiration)
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error checking previous login code",
		})
		return
	}

	// if is not expired, return previous code
	if parsedDate, _ := time.Parse(time.RFC3339, prevLoginCodeExpiration); time.Now().Before(parsedDate.Add(time.Minute * time.Duration(container.Configs().Auth.LoginCodeExpirationTimeMinutes))) {
		c.JSON(successResStatusCode, gin.H{
			"code": prevLoginCode,
		})
		return
	}

	// generate random login code
	loginCode := lib.GenerateLoginCode()

	// storing newly created login code
	stmtSetNewLoginCode, err := db.Prepare("INSERT INTO users_login_codes (user_id, login_code, code_generation_date) VALUES ($1, $2, $3)")
	defer controllers.CloseSafe(stmtSetNewLoginCode)
	_, err = stmtSetNewLoginCode.Exec(userID, loginCode, time.Now().UTC().Format(time.RFC3339))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error registering user login code",
			"msg":     err.Error(),
		})
		return
	}

	// pretend to send login code via sms
	sms := lib.Sms{
		From:    container.Configs().Sms.Number,
		To:      mobile,
		Message: fmt.Sprintf("code: %s", loginCode),
	}
	lib.SendSms(&sms)

	// respond
	c.JSON(successResStatusCode, gin.H{
		"code": loginCode,
	})
}
