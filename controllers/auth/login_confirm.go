package auth

import (
	"database/sql"
	"net/http"
	"time"

	"gitlab.com/xeptore/gipa/container"
	"gitlab.com/xeptore/gipa/controllers"
	"gitlab.com/xeptore/gipa/services/auth"
	"gitlab.com/xeptore/gipa/services/validator"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func LoginConfirm(c *gin.Context) {
	db := container.DBConnection()
	// validating mobile number
	mobile := c.PostForm("mobile")
	code := c.PostForm("code")
	err := validator.ValidateMobile(mobile)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid mobile number",
		})
		return
	}

	// find user by their id
	var userID string
	stmtUserFind, err := db.Prepare("SELECT id FROM users WHERE mobile = $1")
	defer controllers.CloseSafe(stmtUserFind)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error finding user",
		})
		return
	}
	err = stmtUserFind.QueryRow(mobile).Scan(&userID)
	// evaluating user existence
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{})
		return
	} else if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "user with provided mobile number does not exist",
		})
		return
	}

	// find user login code
	var (
		loginCode          string
		codeGenerationDate string
	)
	stmtFindUserLoginCode, err := db.Prepare("SELECT login_code, code_generation_date FROM users_login_codes WHERE user_id = $1 ORDER BY id DESC")
	defer controllers.CloseSafe(stmtFindUserLoginCode)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error finding user login code",
		})
		return
	}
	err = stmtFindUserLoginCode.QueryRow(userID).Scan(&loginCode, &codeGenerationDate)
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{
			"f": err.Error(),
		})
		return
	}

	// evaluating user login code in aspect of its expiration date time
	if parsedDate, err := time.Parse(time.RFC3339, codeGenerationDate); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"g": "g",
		})
		return
	} else if time.Now().After(parsedDate.Add(time.Minute * time.Duration(container.Configs().Auth.LoginCodeExpirationTimeMinutes))) {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "login code has been expired",
		})
		return
	}

	if loginCode != code {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid login code",
		})
		return
	}

	// generating token
	tokenExp := time.Now().Add(time.Hour * 24 * time.Duration(container.Configs().Auth.JWT.ValidationDays)).Format(time.RFC3339)
	claims := jwt.MapClaims{
		"iat": time.Now().Format(time.RFC3339),
		"exp": tokenExp,
		"id":  userID,
	}
	token, err := auth.GenerateToken(claims)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error generating authentication token",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"id": userID,
		"token": gin.H{
			"token": token,
			"exp":   tokenExp,
		},
	})
}
