package controllers

import (
	"database/sql"

	"gitlab.com/xeptore/gipa/services/logger"
)

func CloseSafe(stmtToClose *sql.Stmt) {
	if err := stmtToClose.Close(); err != nil {
		logger.ErrorFatal("error deferring")
	}
}
