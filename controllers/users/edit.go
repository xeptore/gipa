package users

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/xeptore/gipa/container"
	"gitlab.com/xeptore/gipa/controllers"
	"gitlab.com/xeptore/gipa/models"
	"gitlab.com/xeptore/gipa/services/binder"
	"gitlab.com/xeptore/gipa/services/logger"
)

func Edit(c *gin.Context) {
	db := container.DBConnection()
	userID := c.Param("id")
	if len(userID) != 20 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid user id",
		})
		return
	}

	var user models.User
	stmt, err := db.Prepare("SELECT id FROM users WHERE id = $1")
	defer controllers.CloseSafe(stmt)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal server error occurred",
		})
		return
	}

	err = stmt.QueryRow(userID).Scan(&user.ID)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal server error",
		})
		return
	} else if len(user.ID) == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "user does not exist",
		})
		return
	}

	binder.BindUser(&user, c)
	if hasError, err := user.Validate(); hasError {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	stmt, err = db.Prepare("UPDATE users SET first_name = $2, last_name = $3, age = $4, gender = $5, job = $6, email = $7 WHERE id = $1")
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"messsage": "internal error",
		})
		return
	}
	defer controllers.CloseSafe(stmt)

	_, err = stmt.Exec(user.ID, user.FirstName, user.LastName, user.Age, user.Gender, user.Job, user.Email)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{})
		return
	}
	c.JSON(http.StatusNoContent, gin.H{})
}
