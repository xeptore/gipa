package types

type JSONSQL struct {
	JSON string
	SQL  string
}

type JSONModel map[string]JSONSQL

type JSONModels map[string]JSONModel

type ResponseError map[string]string
