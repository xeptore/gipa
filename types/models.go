package types

type ModelConfig struct {
	forms map[string]ModelFormConfig
}

type ModelFormConfig map[string]struct {
	Name     string
	Required bool
}

type ModelSQLConfig map[string]string

func (m *ModelConfig) Forms(f string) ModelFormConfig {
	return m.forms[f]
}

func (m *ModelConfig) AddForm(name string, form *ModelFormConfig) {
	m.forms[name] = *form
}
