package types

type Conf struct {
	Auth Auth `yaml:"auth"`
	DB   DB   `yaml:"database"`
	Sms  Sms  `yaml:"sms"`
}

type DB struct {
	Name     string `yaml:"name"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Host     string `yaml:"host"`
	Port     uint   `yaml:"port"`
}

type Auth struct {
	JWT                            JWT  `yaml:"jwt"`
	LoginCodeExpirationTimeMinutes uint `yaml:"login_code_expiration_minutes"`
	MobileLength                   uint `yaml:"mobile_number_length"`
}

type JWT struct {
	Secret         string `yaml:"secret"`
	ValidationDays uint   `yaml:"token_validation_days"`
}

type Sms struct {
	Number string `yaml:"number"`
}
