package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/xeptore/gipa/container"
	"gitlab.com/xeptore/gipa/services/logger"
	"gitlab.com/xeptore/gipa/services/routing"
)

func main() {
	err := logger.Build()
	if err != nil {
		logger.ErrorFatal("Building logger service")
	}

	err = container.Bootstrap()
	if err != nil {
		logger.ErrorFatal("Bootstrapping")
	}

	engine := gin.Default()
	routing.Build(engine)
	err = engine.Run(":8080")
	if err != nil {
		logger.ErrorFatal(fmt.Sprintf("Running server: %s", err.Error()))
	}
}
