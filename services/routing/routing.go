package routing

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/xeptore/gipa/controllers/auth"
	"gitlab.com/xeptore/gipa/controllers/users"
)

// Build register routes
func Build(engine *gin.Engine) {
	engine.StaticFS("/apiman", http.Dir("./wwwroot/apidoc"))

	api := engine.Group("/api")
	{
		v1 := api.Group("/v1")
		{
			a := v1.Group("/auth")
			{
				a.POST("/login", auth.Login)
				a.POST("/login_confirm", auth.LoginConfirm)
			}
			u := v1.Group("/users")
			{
				u.PATCH("/:id", users.Edit)
			}
		}
	}
}
