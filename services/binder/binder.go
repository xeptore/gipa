package binder

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/xeptore/gipa/container"
	"gitlab.com/xeptore/gipa/models"
)

func BindUser(u *models.User, ctx *gin.Context) {
	userModelJSON := container.Models("User")
	u.FirstName = ctx.PostForm(userModelJSON["FirstName"].JSON)
	u.LastName = ctx.PostForm(userModelJSON["LastName"].JSON)
	u.Email = ctx.PostForm(userModelJSON["Email"].JSON)
	u.Mobile = ctx.PostForm(userModelJSON["Mobile"].JSON)
	u.Job = ctx.PostForm(userModelJSON["Job"].JSON)
	u.Gender = ctx.PostForm(userModelJSON["Gender"].JSON)
	if i, err := strconv.Atoi(ctx.PostForm(userModelJSON["Age"].JSON)); err == nil {
		u.Age = i
	} else {
		u.Age = 0
	}
}
