package registrar

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"

	"gitlab.com/xeptore/gipa/types"
)

func ReadModelForm(file, formName, model string, out *types.ModelConfig) error {
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	var modelFom types.ModelFormConfig
	err = yaml.Unmarshal(bytes, &modelFom)
	if err != nil {
		return err
	}

	out.AddForm(formName, &modelFom)
	return nil
}
