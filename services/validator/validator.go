package validator

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/xeptore/gipa/container"
)

func ValidateMobile(mobile string) error {
	if len(mobile) == int(container.Configs().Auth.MobileLength) {
		if strings.Index(mobile, "9") == 0 {
			_, err := strconv.Atoi(mobile)
			if err == nil {
				return nil
			}
		}
	}
	return errors.New("invalid mobile number")
}
