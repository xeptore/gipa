package auth

import (
	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/xeptore/gipa/container"
)

func GenerateToken(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(container.Configs().Auth.JWT.Secret))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
