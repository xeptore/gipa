package container

import (
	"reflect"

	"gitlab.com/xeptore/gipa/lib"
	"gitlab.com/xeptore/gipa/models"
	"gitlab.com/xeptore/gipa/types"
)

func registerStructsJSON(containerModels *types.JSONModels) {
	registerModel(models.User{}, containerModels)
}

func registerModel(str interface{}, to *types.JSONModels) {
	t := reflect.TypeOf(str)

	(*to)[t.Name()] = make(map[string]types.JSONSQL)

	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		json := field.Tag.Get("json")
		sql := field.Tag.Get("psql")

		if len(json) == 0 {
			json = lib.ToSnakeCase(field.Name)
		}
		if len(sql) == 0 {
			sql = json
		}

		tags := types.JSONSQL{
			JSON: json,
			SQL:  sql,
		}
		(*to)[t.Name()][field.Name] = tags
	}
}
