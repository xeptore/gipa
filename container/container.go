package container

import (
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/xeptore/gipa/services/logger"

	_ "github.com/lib/pq"
	"gitlab.com/xeptore/gipa/config"
	"gitlab.com/xeptore/gipa/types"
)

var configs types.Conf
var modelsJSON = make(types.JSONModels)
var db *sql.DB

func Bootstrap() error {
	err := config.ParseConfigs(&configs)
	if err != nil {
		return err
	}

	registerStructsJSON(&modelsJSON)
	dbConnection, err := connectDatabase()
	if pingErr := dbConnection.Ping(); pingErr != nil || err != nil {
		return errors.New(fmt.Sprintf("unable to connect to database.\n%s\n", pingErr.Error()))
	}
	db = dbConnection
	logger.Success("Successfully connected to database")
	return nil
}

func DBConnection() *sql.DB {
	return db
}

func Models(model string) types.JSONModel {
	return modelsJSON[model]
}

func Configs() *types.Conf {
	return &configs
}
