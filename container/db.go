package container

import (
	"database/sql"
	"fmt"
)

func connectDatabase() (*sql.DB, error) {
	var DBConnectionString = fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		Configs().DB.Host, Configs().DB.Port, Configs().DB.User, Configs().DB.Password, Configs().DB.Name)

	db, err := sql.Open("postgres", DBConnectionString)
	return db, err
}
