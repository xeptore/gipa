package lib

import (
	"log"
)

// Sms type used in send function parameter
type Sms struct {
	Message string
	To      string
	From    string
}

// SendSms message to a receiver
func SendSms(sms *Sms) {
	log.Printf("sms sent. %+v\n", sms)
}
