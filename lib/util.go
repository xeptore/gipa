package lib

import (
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func CheckForError(err error, c *gin.Context, code int) bool {
	if err != nil {
		c.JSON(code, gin.H{})
		return true
	}
	return false
}

func CheckForErrorMessage(err error, c *gin.Context, code int, message string) bool {
	if err != nil {
		c.JSON(code, gin.H{
			"message": message,
		})
		return true
	}
	return false
}

func GenerateLoginCode() string {
	src := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(src)
	out := make([]string, 5)
	out[0] = strconv.Itoa(rnd.Intn(9) + 1)
	for i := 1; i < 5; i++ {
		out[i] = strconv.Itoa(rnd.Intn(10))
	}
	return strings.Join(out, "")
}

func ToSnakeCase(str string) string {
	matchFirstCap := regexp.MustCompile("(.)([A-Z][a-z]+)")
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")

	matchAllCaps := regexp.MustCompile("([a-z0-9])([A-Z])")
	snake = matchAllCaps.ReplaceAllString(snake, "${1}_${2}")

	return strings.ToLower(snake)
}
